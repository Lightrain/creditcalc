package com.lightrain.creditcalc;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class CreditCalcShow extends TabActivity{
	private int m_CreditType;   			// 贷款类型 1商业贷款 2公积金贷款 3组织贷款
	
	private double m_BusinessCreditTotalNum;		// 商业贷款金额，单位：万元
	private double m_BusinessCreditRate;			// 商业贷款年利率
	private double m_BusinessCreditRemain;			// 商业剩余贷款，单位：元
	
	private double m_FundCreditTotalNum;		// 公积金贷款金额，单位：万元
	private double m_FundCreditRate;			// 公积金贷款年利率
	private double m_FundCreditRemain;			// 公积金剩余贷款，单位：元
	
	private int m_CreditTimeLen;   			// 贷款年限，单位：年
	
	static private int REPAY_TYPE_DE_E_BEN_XI  = 1;		//等额本息
	static private int REPAY_TYPE_DE_E_BEN_JIN = 2;     //等额本金
	private int m_RepayType = REPAY_TYPE_DE_E_BEN_XI;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_credit_show);

		getConfigValue();
		
		initShowLayout();
		
		showItemInfo();
	}
	
	private void getConfigValue()
	{
		Intent intent = this.getIntent();
	        
        // 用intent1.getStringExtra()来得到上一个ACTIVITY发过来的字符串。
		m_CreditType = intent.getIntExtra("CreditType", 1);
		
		if ((m_CreditType == 1) || (m_CreditType == 3))
		{
			m_BusinessCreditTotalNum = intent.getDoubleExtra("BusinessCreditTotal", 10);
			m_BusinessCreditRate = intent.getDoubleExtra("BusinessCreditRate", 0.0655);		// 贷款年利率
		}
		
		if ((m_CreditType == 2) || (m_CreditType == 3))
		{
			m_FundCreditTotalNum = intent.getDoubleExtra("FundCreditTotal", 10);
			m_FundCreditRate = intent.getDoubleExtra("FundCreditRate", 0.045);		// 贷款年利率
		}
		
		m_CreditTimeLen = intent.getIntExtra("CreditYear", 30);   // 贷款年限，单位：年
		m_RepayType = intent.getIntExtra("RepayType", 1);
		
	}
	
	private void initShowLayout()
	{
		TabHost tabs = getTabHost(); 
        //设置Tab1   
        TabSpec tab1 = tabs.newTabSpec("tab1");  
        tab1.setIndicator("等额本息");      // 设置tab1的名称   
        tab1.setContent(R.id.tabshow1);    // 关联控件   
        tabs.addTab(tab1);                // 添加tab1   
        
        //设置Tab2   
        TabSpec tab2 = tabs.newTabSpec("tab2");  
        tab2.setIndicator("等额本金");        
        tab2.setContent(R.id.tabshow2);
        tabs.addTab(tab2);

        //tabs.setCurrentTab(m_RepayType);
        if (m_RepayType == REPAY_TYPE_DE_E_BEN_XI)
        {
        	tabs.setCurrentTabByTag("tab1");
        }
        else
        {
        	tabs.setCurrentTabByTag("tab2");
        }
	}
	
	// CreditTotal元，rate月利率，period总期数
	private double calcBenXiMonthRepay(double CreditTotal, double MonthRate, int period)
	{
		double tmpTimes = 1+MonthRate;
		for (int index = 1; index < period; index++)
		{
			tmpTimes = tmpTimes*(1+MonthRate);
		}
		
		double MonthRepay = CreditTotal * MonthRate * tmpTimes / (tmpTimes - 1);
		return MonthRepay;
	}
	
	// CreditPeriodNum贷款期数（年*12）   CreditTotal贷款额度：单位元    CreditRate贷款年利率
	private void calcRepayInfoItemByBENXI(int CreditPeriodNum, double CreditTotal, double CreditRate, double  RepayItem[][])
	{
		//int PeriodNum = m_CreditTimeLen * 12;
		double RepayTotal = 0;
		double RepayPrincipal = 0;
		double RepayRate = 0;
		double CreditRemain = CreditTotal;
		double MonthRate = CreditRate/12;
		double TotalRate = 0;

		////// 计算 等额本息 的结果   //////
		RepayTotal = calcBenXiMonthRepay(CreditTotal, MonthRate, CreditPeriodNum);
		
		// 位置0存放概要信息
		RepayItem[0][0] = RepayTotal;				//最高月供
		RepayItem[0][1] = CreditTotal * MonthRate;	//最高月利息
		
		for (int PeriodIndex = 1; PeriodIndex < CreditPeriodNum; PeriodIndex++)
		{
			RepayRate = CreditRemain * MonthRate;
			RepayPrincipal = RepayTotal - RepayRate;
			CreditRemain = CreditRemain - RepayPrincipal;
			TotalRate = TotalRate + RepayRate;
			
			RepayItem[PeriodIndex][0] = RepayTotal;		//月供
			RepayItem[PeriodIndex][1] = RepayRate;		//月供利息
			RepayItem[PeriodIndex][2] = RepayPrincipal; //月供本金
			RepayItem[PeriodIndex][3] = CreditRemain;	//剩余贷款
		}
		
		RepayRate = CreditRemain * MonthRate;
		RepayPrincipal = CreditRemain;
		RepayTotal = RepayPrincipal + RepayRate;
		CreditRemain = 0;
		TotalRate = TotalRate + RepayRate;
		
		// 最后一个月信息可能略有偏差，需要单独计算
		RepayItem[CreditPeriodNum][0] = RepayTotal;		//月供
		RepayItem[CreditPeriodNum][1] = RepayRate;		//月供利息
		RepayItem[CreditPeriodNum][2] = RepayPrincipal;	//月供本金
		RepayItem[CreditPeriodNum][3] = CreditRemain;	//剩余贷款
		
		// 位置0存放概要信息
		RepayItem[0][2] = TotalRate;	//还总利息
	}
	
	private void calcRepayInfoItemByBENJIN(int CreditPeriodNum, double CreditTotal, double CreditRate, double  RepayItem[][])
	{
		double RepayTotal = 0;
		double RepayPrincipal = 0;
		double RepayRate = 0;
		double CreditRemain = CreditTotal;
		double MonthRate = CreditRate/12;
		double TotalRate = 0;

		////// 计算 等额本金 的结果   //////
		RepayPrincipal = CreditTotal/CreditPeriodNum;
		
		// 位置0存放概要信息
		RepayItem[0][1] = CreditTotal * MonthRate;			//最高月利息
		RepayItem[0][0] = RepayPrincipal + RepayItem[0][1];	//最高月供
		
		for (int PeriodIndex = 1; PeriodIndex < CreditPeriodNum; PeriodIndex++)
		{
			RepayRate = CreditRemain * MonthRate;
			RepayTotal = RepayPrincipal + RepayRate;
			CreditRemain = CreditRemain - RepayPrincipal;
			TotalRate = TotalRate + RepayRate;
			
			RepayItem[PeriodIndex][0] = RepayTotal;		//月供
			RepayItem[PeriodIndex][1] = RepayRate;		//月供利息
			RepayItem[PeriodIndex][2] = RepayPrincipal; //月供本金
			RepayItem[PeriodIndex][3] = CreditRemain;	//剩余贷款
		}
		
		RepayRate = CreditRemain * MonthRate;
		RepayPrincipal = CreditRemain;
		RepayTotal = RepayPrincipal + RepayRate;
		CreditRemain = 0;
		TotalRate = TotalRate + RepayRate;
		
		// 最后一个月信息可能略有偏差，需要单独计算
		RepayItem[CreditPeriodNum][0] = RepayTotal;		//月供
		RepayItem[CreditPeriodNum][1] = RepayRate;		//月供利息
		RepayItem[CreditPeriodNum][2] = RepayPrincipal;	//月供本金
		RepayItem[CreditPeriodNum][3] = CreditRemain;	//剩余贷款
		
		// 位置0存放概要信息
		RepayItem[0][2] = TotalRate;	//还总利息
		
	}
	
	private void showItemInfo()
	{
		String strShowSum, strShowItem;
		TextView tvShowSummary, tvShowItem;
		int PeriodNum = m_CreditTimeLen * 12;
		double  RepayItem[][] = new double[361][4]; // 分配的空间大小   
		

		////// 先显示 等额本息 的结果   //////
		if (m_CreditType == 1)
		{
			calcRepayInfoItemByBENXI(PeriodNum, m_BusinessCreditTotalNum*10000, m_BusinessCreditRate, RepayItem);
			strShowSum = String.format("商业贷款：%.0f万 贷款利率：%.3f\n", m_BusinessCreditTotalNum, 100*m_BusinessCreditRate);
		}
		else if (m_CreditType == 2)
		{
			calcRepayInfoItemByBENXI(PeriodNum, m_FundCreditTotalNum*10000, m_FundCreditRate, RepayItem);
			strShowSum = String.format("公积金贷款：%.0f万 贷款利率：%.3f\n", m_FundCreditTotalNum, 100*m_FundCreditRate);
		}
		else // (m_CreditType == 3)
		{
			calcRepayInfoItemByBENXI(PeriodNum, m_BusinessCreditTotalNum*10000, m_BusinessCreditRate, RepayItem);
			strShowSum = String.format("商业贷款：%.0f万 贷款利率：%.3f\n", m_BusinessCreditTotalNum, 100*m_BusinessCreditRate);
			
			double  RepayItem2[][] = new double[361][4]; // 分配空间大小   ;
			calcRepayInfoItemByBENXI(PeriodNum, m_FundCreditTotalNum*10000, m_FundCreditRate, RepayItem2);
			strShowSum = strShowSum + String.format("公积金贷款：%.0f万 贷款利率：%.3f\n", m_FundCreditTotalNum, 100*m_FundCreditRate);
			
			for (int i = 0; i<361; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					RepayItem[i][j] = RepayItem[i][j] + RepayItem2[i][j];
				}
			}
		}

		tvShowSummary = (TextView)this.findViewById(R.id.tvShowSummary);
		strShowSum = strShowSum + String.format("总利息:%.2f万\t每月月供:%.2f\n总还款:%.2f万\t最高月利:%.2f", 
				RepayItem[0][2]/10000, RepayItem[0][0], RepayItem[0][2]/10000+m_BusinessCreditTotalNum, RepayItem[0][1]);
		tvShowSummary.setText(strShowSum);
		
		strShowItem = String.format("期次  本息  利息  本金  剩余本金");
		for (int PeriodIndex = 1; PeriodIndex < PeriodNum; PeriodIndex++)
		{
			strShowItem = strShowItem + String.format("\n%d\t %.2f\t %.2f\t %.2f\t %.1f万", 
					PeriodIndex, RepayItem[PeriodIndex][0], RepayItem[PeriodIndex][1], RepayItem[PeriodIndex][2], RepayItem[PeriodIndex][3]/10000);
		}

		tvShowItem = (TextView)this.findViewById(R.id.tvShowItem);
		tvShowItem.setMovementMethod(ScrollingMovementMethod.getInstance());
		tvShowItem.setMaxLines(20);
		tvShowItem.setText(strShowItem);
		//////////////////////////////////

		////// 再计算 等额本金 的结果   //////
		if (m_CreditType == 1)
		{
			calcRepayInfoItemByBENJIN(PeriodNum, m_BusinessCreditTotalNum*10000, m_BusinessCreditRate, RepayItem);
		}
		else if (m_CreditType == 2)
		{
			calcRepayInfoItemByBENJIN(PeriodNum, m_FundCreditTotalNum*10000, m_FundCreditRate, RepayItem);
		}
		else // (m_CreditType == 3)
		{
			calcRepayInfoItemByBENJIN(PeriodNum, m_BusinessCreditTotalNum*10000, m_BusinessCreditRate, RepayItem);
			
			double  RepayItem2[][] = new double[361][4]; // 分配空间大小   ;
			calcRepayInfoItemByBENJIN(PeriodNum, m_FundCreditTotalNum*10000, m_FundCreditRate, RepayItem2);
			
			for (int i = 0; i<361; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					RepayItem[i][j] = RepayItem[i][j] + RepayItem2[i][j];
				}
			}
		}

		tvShowSummary = (TextView)this.findViewById(R.id.tvShowSummary2);
		strShowSum = String.format("总利息：%.2f万\t最高月供：%.2f元\n总还款：%.2f万元\t最高月利：%.2f元", 
				RepayItem[0][2]/10000, RepayItem[0][0], RepayItem[0][2]/10000+m_BusinessCreditTotalNum, RepayItem[0][1]);
		tvShowSummary.setText(strShowSum);
		
		
		strShowItem = String.format("期次  本息  利息  本金  剩余本金");
		for (int PeriodIndex = 1; PeriodIndex < PeriodNum; PeriodIndex++)
		{
			strShowItem = strShowItem + String.format("\n%d\t %.2f\t %.2f\t %.2f\t %.1f万", 
					PeriodIndex, RepayItem[PeriodIndex][0], RepayItem[PeriodIndex][1], RepayItem[PeriodIndex][2], RepayItem[PeriodIndex][3]/10000);
		}

		tvShowItem = (TextView)this.findViewById(R.id.tvShowItem2);
		tvShowItem.setMovementMethod(ScrollingMovementMethod.getInstance());
		tvShowItem.setMaxLines(20);
		tvShowItem.setText(strShowItem);
		//////////////////////////////////
	}
}
