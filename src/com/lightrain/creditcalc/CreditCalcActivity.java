package com.lightrain.creditcalc;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class CreditCalcActivity extends TabActivity implements OnClickListener, OnTabChangeListener, OnItemSelectedListener {

	private int m_CreditType;		//1商业贷款， 2公积金贷款，3组合贷款
	private double m_BussinessCreditTotalNum;	// 商业贷款金额，单位：万元
	private double m_BussinessCreditRate;		// 商业贷款利率
	
	private double m_FundCreditTotalNum;	// 公积金贷款金额，单位：万元
	private double m_FundCreditRate;		// 公积金贷款利率
	
	private int m_CreditTimeLen;   	// 贷款年限，单位：年
	private int m_RepayType;		// 还款方式     1等额本息     2等额本金
	
	static private int ERROR = 1;
	static private int OK = 0;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_credit_calc);

		initCalcLayout();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.credit_calc, menu);
		return true;
	}
	
	private void initCalcLayoutCredit()
	{
        /////////////////////    设置贷款期限      /////////////////////  
		Spinner spCreditTimeLen = (Spinner)this.findViewById(R.id.spinnerCreditTimeLen);
        //将可选内容与ArrayAdapter连接起来  
        ArrayAdapter<String> adapterTimeLen = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        
        for (int i=30; i>0; i--)
        {
        	adapterTimeLen.add(String.format("%d年(%d期)", i, i*12));
        }

        
        //设置下拉列表的风格  
        adapterTimeLen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
          
        //将adapter 添加到spinner中  
        spCreditTimeLen.setAdapter(adapterTimeLen);
        //m_spRentCfgOperate.setOnItemSelectedListener(this);
		/////////////////////    设置贷款期限      ///////////////////// 
		        
		/////////////////////    设置商业贷款利率      /////////////////////
        Spinner spCreditRate = (Spinner)this.findViewById(R.id.spinnerBussinessRate);     //利率
        //将可选内容与ArrayAdapter连接起来  
        ArrayAdapter<String> adapterRate = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterRate.add("手工输入");
        adapterRate.add("最新基准(6.55%)");
          
        //设置下拉列表的风格  
        adapterRate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
          
        //将adapter 添加到spinner中  
        spCreditRate.setAdapter(adapterRate);
        spCreditRate.setOnItemSelectedListener(this);
        spCreditRate.setSelection(1);
		/////////////////////    设置商业贷款利率      /////////////////////  
        
		/////////////////////    设置商业贷款利率折扣      ///////////////////// 
        Spinner spCreditRateDiscount = (Spinner)this.findViewById(R.id.spinnerBussinessDiscount);
        //将可选内容与ArrayAdapter连接起来  
        ArrayAdapter<String> adapterRateDiscount = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterRateDiscount.add("无折扣");
        adapterRateDiscount.add("1.1");
          
        //设置下拉列表的风格  
        adapterRateDiscount.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
          
        //将adapter 添加到spinner中  
        spCreditRateDiscount.setAdapter(adapterRateDiscount);
        spCreditRateDiscount.setOnItemSelectedListener(this);
		/////////////////////    设置商业贷款利率折扣      ///////////////////// 
        
		/////////////////////    设置商业贷款利率      /////////////////////
        Spinner spFundCreditRate = (Spinner)this.findViewById(R.id.spinnerFundRate);     //利率
		//将可选内容与ArrayAdapter连接起来  
		ArrayAdapter<String> adapterFundRate = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		adapterFundRate.add("手工输入");
		adapterFundRate.add("最新基准(4.5%)");
		
		//设置下拉列表的风格  
		adapterFundRate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
		
		//将adapter 添加到spinner中  
		spFundCreditRate.setAdapter(adapterFundRate);
		spFundCreditRate.setOnItemSelectedListener(this);
		spFundCreditRate.setSelection(1);
		/////////////////////    设置商业贷款利率      ///////////////////// 
     
        Button btn = (Button)this.findViewById(R.id.btnBussinessCalc);
        btn.setOnClickListener(this);
	}

	private void initCalcLayout()
	{
		TabHost tabs = getTabHost(); 
        //设置Tab1   
        TabSpec tab1 = tabs.newTabSpec("tab1");  
        tab1.setIndicator("商业贷款");      // 设置tab1的名称   
        tab1.setContent(R.id.tab1);    // 关联控件   
        tabs.addTab(tab1);                // 添加tab1   
        
        //设置Tab2   
        TabSpec tab2 = tabs.newTabSpec("tab2");  
        tab2.setIndicator("公积金贷款");        
        tab2.setContent(R.id.tab1);      
        tabs.addTab(tab2);
        
        //设置Tab3 
        TabSpec tab3 = tabs.newTabSpec("tab3");  
        tab3.setIndicator("组合贷款");        
        tab3.setContent(R.id.tab3);      
        tabs.addTab(tab3);

		//设置TAB变更监听函数
		tabs.setOnTabChangedListener(this);
		onTabChanged("tab1");
		
		//初始化贷款显示界面
		initCalcLayoutCredit();
	}

	@Override
	public void onTabChanged(String tabIDStr) {
		// TODO Auto-generated method stub
		LinearLayout LayoutBusinessTotal = (LinearLayout)this.findViewById(R.id.layoutBusinessTotal);
		LinearLayout LayoutBusinessRate = (LinearLayout)this.findViewById(R.id.layoutBusinessRate);
		LinearLayout LayoutBusinessRateDiscount = (LinearLayout)this.findViewById(R.id.layoutBusinessRateDiscount);
		
		LinearLayout LayoutFundTotal = (LinearLayout)this.findViewById(R.id.layoutFundTotal);
		LinearLayout LayoutFundRate = (LinearLayout)this.findViewById(R.id.layoutFundRate);

		
		if (tabIDStr.equals("tab1")) {
			m_CreditType = 1;	//商业贷款
			
			LayoutBusinessTotal.setVisibility(View.VISIBLE);
			LayoutBusinessRate.setVisibility(View.VISIBLE);
			LayoutBusinessRateDiscount.setVisibility(View.VISIBLE);
			LayoutFundTotal.setVisibility(View.GONE);
			LayoutFundRate.setVisibility(View.GONE);
		}
		else if (tabIDStr.equals("tab2"))
		{
			m_CreditType = 2;	//公积金贷款
			
			LayoutBusinessTotal.setVisibility(View.GONE);
			LayoutBusinessRate.setVisibility(View.GONE);
			LayoutBusinessRateDiscount.setVisibility(View.GONE);
			LayoutFundTotal.setVisibility(View.VISIBLE);
			LayoutFundTotal.setBackgroundResource(R.drawable.setting_list_top);
			
			LinearLayout.LayoutParams lp = (LayoutParams) LayoutBusinessTotal.getLayoutParams();
			LayoutFundTotal.setLayoutParams(lp);
			
			LayoutFundRate.setVisibility(View.VISIBLE);
		}
		else if (tabIDStr.equals("tab3")) 
		{
			m_CreditType = 3;	// 组合贷款
			LayoutBusinessTotal.setVisibility(View.VISIBLE);
			LayoutBusinessRate.setVisibility(View.VISIBLE);
			LayoutBusinessRateDiscount.setVisibility(View.VISIBLE);
			LayoutFundTotal.setVisibility(View.VISIBLE);
			LayoutFundTotal.setBackgroundResource(R.drawable.setting_list_middle);
			
			LinearLayout.LayoutParams lp = (LayoutParams) LayoutBusinessRate.getLayoutParams();
			LayoutFundTotal.setLayoutParams(lp);
			
			LayoutFundRate.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int viewId = v.getId();
		Intent intent;
		switch (viewId)
		{
			case R.id.btnBussinessCalc:
				if (OK != getCalcCreaditInfo())
				{
					break;
				}
				
				//Intent intent = new Intent(this, CreditCalcShow.class);   
				intent = new Intent(); 
				
				//用intent.putExtra(String name, String value);来传递参数。
				intent.putExtra("CreditType", (int)m_CreditType);
				
				if ((m_CreditType == 1) || (m_CreditType == 3))
				{
					intent.putExtra("BusinessCreditTotal", (double)m_BussinessCreditTotalNum);
					intent.putExtra("BusinessCreditRate", (double)m_BussinessCreditRate);
				}
				
				if ((m_CreditType == 2) || (m_CreditType == 3))
				{
					intent.putExtra("FundCreditTotal", (double)m_FundCreditTotalNum);
					intent.putExtra("FundCreditRate", (double)m_FundCreditRate);
				}
				intent.putExtra("CreditYear", (int)m_CreditTimeLen);
				intent.putExtra("RepayType", (int)m_RepayType);
				intent.setClass(this, CreditCalcShow.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); 
				startActivity(intent);
				break;

			default:
				break;
		}
	}
	
	private int getCalcCreaditInfo()
	{
		String strTmp;
		EditText etTmp;
		
		
		// 商业贷款 和 组合贷款时，需要计算商业贷款相关信息
		if ((m_CreditType == 1) || (m_CreditType == 3))
		{
			// 商业贷款总金额
			etTmp = (EditText)this.findViewById(R.id.etBussinessCreditTotal);
			strTmp = etTmp.getText().toString();
			try { 
				m_BussinessCreditTotalNum = Double.parseDouble(strTmp);	// 贷款金额，单位：万元
				} 
			catch(IllegalArgumentException ex) { 
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("错误");
				builder.setMessage("请输入正确贷款总额！");
				builder.setPositiveButton("确认", null);
				builder.create().show();
				
				etTmp.setBackgroundColor(Color.RED);
			    return ERROR;
			}
			etTmp.setBackgroundColor(Color.WHITE);
			
			// 商业贷款利息
			etTmp = (EditText)this.findViewById(R.id.etBussinessRate);
			strTmp = etTmp.getText().toString();
			try { 
				m_BussinessCreditRate = Double.parseDouble(strTmp); 
				} 
			catch(IllegalArgumentException ex) { 
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("错误");
				builder.setMessage("请输入正确贷款利率！");
				builder.setPositiveButton("确认", null);
				builder.create().show();
				
				etTmp.setBackgroundColor(Color.RED);
			    return ERROR;
			}
			m_BussinessCreditRate = m_BussinessCreditRate/100;
			etTmp.setBackgroundColor(Color.WHITE);
		}
		
		// 公积金贷款 和 组合贷款时，需要计算公积金贷款相关信息
		if ((m_CreditType == 2) || (m_CreditType == 3))
		{
			// 商业贷款总金额
			etTmp = (EditText)this.findViewById(R.id.etFundCreditTotal);
			strTmp = etTmp.getText().toString();
			try { 
				m_FundCreditTotalNum = Double.parseDouble(strTmp);	// 贷款金额，单位：万元
				} 
			catch(IllegalArgumentException ex) { 
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("错误");
				builder.setMessage("请输入正确贷款总额！");
				builder.setPositiveButton("确认", null);
				builder.create().show();
				
				etTmp.setBackgroundColor(Color.RED);
			    return ERROR;
			}
			etTmp.setBackgroundColor(Color.WHITE);
			
			// 商业贷款利息
			etTmp = (EditText)this.findViewById(R.id.etFundRate);
			strTmp = etTmp.getText().toString();
			try { 
				m_FundCreditRate = Double.parseDouble(strTmp); 
				} 
			catch(IllegalArgumentException ex) { 
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("错误");
				builder.setMessage("请输入正确贷款利率！");
				builder.setPositiveButton("确认", null);
				builder.create().show();
				
				etTmp.setBackgroundColor(Color.RED);
			    return ERROR;
			}
			m_FundCreditRate = m_FundCreditRate/100;
			etTmp.setBackgroundColor(Color.WHITE);
		}
		
		Spinner spCreditTimeLen = (Spinner)this.findViewById(R.id.spinnerCreditTimeLen); 
		m_CreditTimeLen = 30-(int)spCreditTimeLen.getSelectedItemId();   		// 贷款年限，单位：年

		RadioGroup rgRepayType = (RadioGroup)this.findViewById(R.id.radioGroupRepayType);
		int radioBTNID = rgRepayType.getCheckedRadioButtonId();
		if (radioBTNID == R.id.radioRepayBenxi)
		{
			m_RepayType = 1;				// 还款方式     1等额本息     2等额本金
		}
		else
		{
			m_RepayType = 2;				// 还款方式     1等额本息     2等额本金
		}
		
		return OK;
	}

	private double getCreditRate()
	{
		Spinner spTmp = (Spinner)this.findViewById(R.id.spinnerBussinessRate);
		String strTmp = spTmp.getSelectedItem().toString();
		if (0 == strTmp.compareTo("手工输入"))
		{
			return 0;
		}
		
		int start = strTmp.indexOf('(');
		strTmp = strTmp.substring(strTmp.indexOf('(')+1, strTmp.indexOf(')')-1);
		return Double.parseDouble(strTmp.toString());
	}
	
	private double getCreditRateDiscount()
	{
		Spinner spTmp = (Spinner)this.findViewById(R.id.spinnerBussinessDiscount);
		String strTmp = spTmp.getSelectedItem().toString();
		if (0 == strTmp.compareTo("无折扣"))
		{
			return 1.0;
		}
		
		return Double.parseDouble(strTmp);
	}

	@Override
	public void onItemSelected(AdapterView<?> adapter,View view,int position,long id) {
		// TODO Auto-generated method stub
		//int viewId = view.getId();
		int iSpinnerID = ((Spinner)adapter).getId();
		EditText etTmp; 
		String strTmp;
		
		switch(iSpinnerID)
		{
			case R.id.spinnerBussinessRate:
				etTmp = (EditText)this.findViewById(R.id.etBussinessRate);
				LinearLayout layoutTmp = (LinearLayout)this.findViewById(R.id.layoutBusinessRateDiscount);
				
				//如果是“手工输入”
				if (0 == position)
				{
					layoutTmp.setVisibility(View.GONE);
					etTmp.setText("0.0");
					etTmp.setEnabled(true);
				}
				else
				{
					layoutTmp.setVisibility(View.VISIBLE);
					etTmp.setText(String.format("%f", getCreditRate()*getCreditRateDiscount()));
					etTmp.setEnabled(false);
				}
				
				break;
				
			case R.id.spinnerBussinessDiscount:
				etTmp = (EditText)this.findViewById(R.id.etBussinessRate);
				etTmp.setText(String.format("%f", getCreditRate()*getCreditRateDiscount()));
				break;
				
			case R.id.spinnerFundRate:
				etTmp = (EditText)this.findViewById(R.id.etFundRate);
				//如果是“手工输入”
				if (0 == position)
				{
					etTmp.setText("0.0");
					etTmp.setEnabled(true);
				}
				else
				{
					strTmp = adapter.getSelectedItem().toString();
					int start = strTmp.indexOf('(');
					strTmp = strTmp.substring(strTmp.indexOf('(')+1, strTmp.indexOf(')')-1);
					etTmp.setText(strTmp);
					etTmp.setEnabled(false);
				}
				break;
				
			default:
				break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
